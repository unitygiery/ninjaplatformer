﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public string levelToLoad;
    public KeyCode key;

    void Update()
    {
        if (Input.GetKeyDown(key) && Application.loadedLevelName == "sc1")
        {
            //Application.LoadLevel("menu");
            Application.Quit();
        }
    }

    public void LoadScene(string level)
    {
        Application.LoadLevel(level);
    }

    public void LoadScene()
    {
        if(!levelToLoad.Equals(null))
            Application.LoadLevel(levelToLoad);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
