﻿using UnityEngine;
using System.Collections;

public class DestroyOnTime : MonoBehaviour {

    public float boomTime;
    private float time;

	// Use this for initialization
	void Start () {
        time = boomTime;
	}
	
	// Update is called once per frame
	void Update () {
        if (time > 0)
            time -= Time.deltaTime;
        else
            Destroy(gameObject);
	}
}
