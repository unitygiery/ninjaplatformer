﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    private Slider slider;
    public Text text;
    public PlayerController player;

	// Use this for initialization
    void Start()
    {
        slider = GetComponent<Slider>();
        slider.maxValue = player.maxHp;
        slider.value = slider.maxValue;
        text.text = "Player " + player.id + ":  " + slider.value + " / " + slider.maxValue;
    }
	
	// Update is called once per frame
	void Update () {
        slider.value = player.hp;
        text.text = "Player " + player.id + ":  " + slider.value + " / " + slider.maxValue;
	}


}
