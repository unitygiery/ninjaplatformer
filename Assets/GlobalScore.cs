﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GlobalScore : MonoBehaviour {

    private Text text;
    public PlayerController p1;
    public PlayerController p2;
    public float resetTime;
    private float time;
    private int score1;
    private int score2;
	// Use this for initialization
	void Start () {
        score1 = 0;
        score2 = 0;
        text = GetComponent<Text>();
        time = resetTime;
	}
	
	// Update is called once per frame
	void Update () {
        if (time == resetTime)
        {
            score1 = p2.getDeaths();
            score2 = p1.getDeaths();
        } 
        text.text = score1 + " : " + score2;
        if (p1.getDed() || p2.getDed())
        {
            if (time > 0)
                time -= Time.deltaTime;
            else
            {
                p1.Revive();
                p2.Revive();
                time = resetTime;
            }
        }
        
	}
}
